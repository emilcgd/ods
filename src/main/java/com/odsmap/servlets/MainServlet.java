package com.odsmap.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class MainServlet extends HttpServlet {

    String name = "world";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("UTf-8");
        response.getOutputStream().write(getPage(getYandexMap()));
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    private byte[] getPage(String text) {
        try {
            return text.getBytes("UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getYandexMap(){
        String html = "<html>"
                      + "<head>"
                      + "<title>"
                      + "yandex-med-karta"
                      + "</title>"
                      + "<script src=\"https://api-maps.yandex.ru/2.1/?apikey=85ba9fdc-131d-4acf-9b9c-5ca909780cec&lang=ru_RU\" type=\"text/javascript\"></script>"
                      + "<script charset=\"utf-8\" src=\"script.js\"></script>"
                      + "</head>"
                      + "<body>"
                      + "123"
                      + "<div id=\"map\" style=\"width: 1000px; height: 800px\"></div>"
                      + "</body>";
        html += "</html>";
        return html;
    }

//    private String getOSMMap(){
//        String html = "<html>"
//                + "<head>"
//                + "<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/css/ol.css\" type=\"text/css\">"
//                + "<script src=\"https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/build/ol.js\"></script>"
//                + "<script charset=\"utf-8\" src=\"osm-script.js\"></script>"
//                + "<script src=\"https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList\"></script>"
//                + "<title>OSM-med-karta</title>"
//                + "</head>"
//                + "<body>"
//                + "123"
//                +  "<script src=\"https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/build/ol.js\"></script>"
//                + "<div id=\"map\" class=\"map\" style=\"width: 1000px; height: 800px\"></div>"
//                + "</body>";
//        html += "</html>";
//        return html;
//    }

    private String getOSMMap(){

        String html = "<html>"
                + "<head>"
                + "<script src=\"http://www.openlayers.org/api/OpenLayers.js\"></script>"
                + "<script type=\"text/javascript\">"
                + "<script charset=\"utf-8\" src=\"osm-script.js\"></script>"
                + "<title>OSM-med-karta</title>"
                + "</head>"
                + "<body onload=\"GetMap();\">"
                + "123"
                + "<div id=\"OSMap\" style=\"width: 1000px; height: 800px\"></div>"
                + "</body>";
        html += "</html>";
        return html;
    }
//aseaseaseaseaseasease
}
