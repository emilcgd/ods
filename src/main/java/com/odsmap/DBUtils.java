package com.odsmap;

import com.odsmap.domain.Coords;
import org.postgis.Point;
import org.postgis.Polygon;
import org.postgresql.geometric.PGpoint;
import org.postgresql.geometric.PGpolygon;

import javax.xml.crypto.dsig.keyinfo.PGPData;
import java.awt.*;
import java.io.*;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.postgis.*;

public class DBUtils {


    public static String fileName = "./src/main/java/com/odsmap/scripts/borders/bash-full.txt";
    private static DBUtils dbUtils;
    private static Connection conn;
    private static ArrayList<String> al = new ArrayList<String>();


    public static DBUtils getInstance() {
        if (dbUtils == null) {
            dbUtils = new DBUtils();
        }
        try {
            if (dbUtils.getConn() == null || dbUtils.getConn().isClosed()) {
                dbUtils.dbConnectPostgreSQL();
                ((org.postgresql.PGConnection) conn).addDataType("geometry", Class.forName("org.postgis.PGgeometry"));
                ((org.postgresql.PGConnection) conn).addDataType("box3d", Class.forName("org.postgis.PGbox3d"));
                System.out.println("Соединение восстановлено.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dbUtils;
    }


    //connect to MySQL
    public boolean dbConnectMySQL() {
        boolean bool = false;
        String url = "jdbc:mysql://localhost/ods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&autoReconnect=true";
        String user = "root";
        String password = "";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected");
            bool = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            bool = false;
        }
        return bool;
    }

    //    connect to PostgreSQL
    public boolean dbConnectPostgreSQL() {
        boolean bool = false;
        String url = "jdbc:postgresql://127.0.0.1:5432/ods";
        String user = "postgres";
        String password = "root";
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected");
            bool = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Not connected");
            System.out.println(ex);
            bool = false;
        }
        return bool;
    }

    public Connection getConn() {
        return conn;
    }

    //Создание таблицы с координатами РБ
    public void createRegionRB() {
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS regionRB(id SERIAL PRIMARY KEY, tip TEXT NOT NULL, coordinates geometry NOT NULL)");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //Ввод данных в Таблицу - deprecated
    public static boolean insertRegionRB(String tip) {
//        getRegionCoordinatesFromFile();


        Polygon geo = new Polygon(
                new LinearRing[] {
                        new LinearRing(
                                new Point[] {
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                        new Point(3.2, 2.4),
                                }
                        )
                }
        );


        try (PreparedStatement pSt = conn.prepareStatement("INSERT INTO regionRB(tip, coordinates) VALUES (?, ?)")) {
            ((org.postgresql.PGConnection) conn).addDataType("geometry", Class.forName("org.postgis.PGgeometry"));
            pSt.setString(1, tip);
            pSt.setObject(2, new PGgeometry(geo));
            return pSt.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }


    //Запись координат РБ в массив - deprecated
    public static Polygon getRegionCoordinatesFromFile() {
        Polygon geo = new Polygon();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = "";
            geo = new Polygon(new LinearRing[]{new LinearRing(new Point[]{new Point()})});

            while ((line = reader.readLine()) != null) {
                String[] strings = line.split(",");
                String strX = strings[0];
                String strY = strings[1];
                org.postgis.Point p = new org.postgis.Point();
                p.setX(Double.valueOf(strX));
                p.setY(Double.valueOf(strY));
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return geo;
    }


    //Получение координат из БД и вывод в консоль
    public void getCoordinatesById(long id) {
        try (Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery("SELECT * FROM regionRB WHERE Id = " + id);
            while (rs.next()) {
                PGgeometry geom = (PGgeometry) rs.getObject(3);
                System.out.println("Row " + id + ":");
                System.out.println(geom.toString());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //Получение координат из БД и Экспорт в Яндекс API
    public static void getCoordinatesByIdExportToAPI(long id) {
        ArrayList<String> al = new ArrayList<>();
        PGgeometry geom;
        try (Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery("SELECT * FROM regionRB WHERE Id = " + id);
            while (rs.next()) {
                geom = (PGgeometry) rs.getObject(3);
                    Polygon poly = (Polygon) geom.getGeometry();
                    for (int r = 0; r < poly.numRings(); r++){
                        LinearRing rng = poly.getRing(r);
                        System.out.println("Ring : " + r);
                        for (int p = 0; p < rng.numPoints(); p++) {
                            Point pt = rng.getPoint(p);
                            System.out.println("Point : " + p);
                            al.add(pt.toString());
                        }
                    }


            }
            for (String s : al)
                System.out.println(s);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
