package com.odsmap;

import com.odsmap.servlets.MainServlet;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import sun.applet.Main;

import javax.servlet.Servlet;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;


public class Launcher {

    public static void main(String[] args) throws SQLException {
        initServer();
    }

    public static void initServer() {
        int port = 8082;
        Server server = new Server(port);

        ServletContextHandler ctx = new ServletContextHandler();
        ctx.setContextPath("/");

        DefaultServlet defaultServlet = new DefaultServlet();
        ServletHolder holderPwd = new ServletHolder("default", defaultServlet);
        holderPwd.setInitParameter("resourceBase", "./src/main/java/com/odsmap/scripts");

        ctx.addServlet(holderPwd, "/*");
        ctx.addServlet(MainServlet.class, "/inv");

        server.setHandler(ctx);
        startingMethods();
        DBUtils.insertRegionRB("1");



        try {
            server.start();
            System.out.println("Listening port : " + port);
            server.join();
            startingMethods();
        } catch(Exception ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }
    }

    public static void startingMethods() {
        DBUtils.getInstance().createRegionRB();
        //DBUtils.getInstance().getCoordinatesById(1);
        DBUtils.getInstance().getCoordinatesByIdExportToAPI(1);
    }


}
